import React, { useState, useRef, useEffect } from 'react'
import { Form } from 'antd'
import { Formik } from 'formik'
import TableComponent from './TableComponent'

const LOCAL_STORAGE_KEY = 'userApp.users'

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

export default function App() {
  const [users, setUsers] = useState([{}])
  const userNameRef = useRef()
  const userMailRef = useRef()
  const userPhoneRef = useRef()

  useEffect(() => {
    
      const storedusers = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
      if (storedusers) setUsers(storedusers)

  }, [])

  useEffect(() => {
      localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(users))
  }, [users])
  function handleAdduser(e) {
      const name = userNameRef.current.value
      const mail = userMailRef.current.value
      const phone = userPhoneRef.current.value
      if (name === '' || mail === '' || phone === '') return
      setUsers(prevusers => {
          return [...prevusers, { id: phone, name: name, mail:mail}]
      })


  }
  const deleteuser = (id) => {
      setUsers(users.filter((user) => user.id !== id))
  }

  return (
    <Formik
    {...layout}
      
      initialValues={{ name: 'hello', mail: 20, newsletter: false }}
      render={() => (
        <Form>
          <div>

<p>Name:<input name="name" ref={userNameRef} type="text" /></p>
<p>Email:<input name="email" ref={userMailRef} type="text" /></p>
<p>Phone:<input name="phone" ref={userPhoneRef} type="number" /></p>
<span><textarea type="text" /></span>
<button {...tailLayout} onClick={handleAdduser}>Add user</button>
<TableComponent users={users} deleteuser={deleteuser} />


</div>
        </Form>
      )}
    />
  )
}